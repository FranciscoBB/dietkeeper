package cat.ieseugeni;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import cat.ieseugeni.api.ApiService;
import cat.ieseugeni.api.NetworkClient;
import cat.ieseugeni.model.LoginModel;
import cat.ieseugeni.utils.CryptoUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Activitat que s'encarrega de la comprovació, donada d'alta, i inici de sessió.
 */
public class ActivityLogin extends AppCompatActivity {

    private final String TAG = "USUARI";

    Retrofit retrofit;

    LoginModel loginModel;
    private EditText etUsuari, etPassword;
    Button btSignup, btLogin;

    private Intent intent;
    private Bundle bundle = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuari = findViewById(R.id.etUsuari);
        etPassword = findViewById(R.id.etPassword);

        btSignup = findViewById(R.id.btSignup);
        btLogin = findViewById(R.id.btLogin);

        btSignup.setOnClickListener(v -> {
            CryptoUtils.encripta(etPassword.getText().toString());
            compararLogin(etUsuari.getText().toString(), CryptoUtils.encripta(etPassword.getText().toString()));
            inserirLogin();
        });

        btLogin.setOnClickListener(v -> {
            //Si s'executa correctament la crida de comparar usuari, trasllada cap a la següent pantalla, si no, adverteix a l'usuari.
            if (compararLogin(etUsuari.getText().toString(), CryptoUtils.encripta(etPassword.getText().toString())).isCanceled()) {
                Toast.makeText(getApplicationContext(), "No s'ha pogut connectar amb el servidor...", Toast.LENGTH_LONG).show();
            } else {
                intent = new Intent(ActivityLogin.this, DietKeeper.class);
                bundle.putString("user", etUsuari.getText().toString());
                intent.putExtra("bundle1", bundle);
                startActivity(intent);
                onStop();

            }
        });
    }

    /**
     * Crida que compara si l'usuari i la seva contrasenya existeixen en la BBDD
     *
     * @param usuari Nom d'usuari
     * @param pass   Contrasenya
     * @return Retorna la crida.
     */
    private Call<List<LoginModel>> compararLogin(String usuari, String pass) {
        retrofit = NetworkClient.getRetrofitClient();
        ApiService service = retrofit.create(ApiService.class);
        Call<List<LoginModel>> loginResponseCall = service.compararLogin(usuari, pass);

        loginResponseCall.enqueue(new Callback<List<LoginModel>>() {
            @Override
            public void onResponse(Call<List<LoginModel>> call, Response<List<LoginModel>> response) {
                if (response.isSuccessful()) {

                    Log.d(TAG, " Usuari: " + usuari);

                    //Si el resultat de la Query retorna l'usuari...
                    if (compararLogin(etUsuari.getText().toString(), CryptoUtils.encripta(etPassword.getText().toString())) == (service.compararLogin(etUsuari.getText().toString(), CryptoUtils.encripta(etPassword.getText().toString())))) {
                        Toast.makeText(getApplicationContext(), "L'usuari ja existeix!", Toast.LENGTH_SHORT).show();
                        //Si no...
                    } else {
                        Toast.makeText(getApplicationContext(), "El nom d'usuari és disponible!", Toast.LENGTH_LONG).show();
                    }
                    etUsuari.getText().clear();
                    etPassword.getText().clear();
                    etUsuari.requestFocus();
                }
            }

            @Override
            public void onFailure(Call<List<LoginModel>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "No s'ha pogut connectar amb la base de dades", Toast.LENGTH_LONG).show();
            }
        });
        return loginResponseCall;
    }

    /**
     * Crida que insereix un usuari i la seva contrasenya a la BBDD
     *
     * @return Retorna la crida.
     */
    private Call<LoginModel> inserirLogin() {
        retrofit = NetworkClient.getRetrofitClient();
        ApiService service = retrofit.create(ApiService.class);
        new LoginModel();
        loginModel.setId(0);
        loginModel.setUsuari(etUsuari.getText().toString());
        loginModel.setPass(CryptoUtils.encripta(etPassword.getText().toString()));

        Call<LoginModel> loginResponseCall = service.inserirLogin(loginModel);

        loginResponseCall.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {

                if (response.isSuccessful()) {
                    Log.d(TAG, " Usuari: " + etUsuari.getText().toString());

                    //Si el resultat de la Query no retorna res, O MILLOR DIT... RETORNA BUIT
                    if (compararLogin(etUsuari.getText().toString(), CryptoUtils.encripta(etPassword.getText().toString())) != (service.compararLogin(etUsuari.getText().toString(), CryptoUtils.encripta(etPassword.getText().toString())))) {
                        //Inserta l'usuari.
                        Toast.makeText(getApplicationContext(), "Usuari inserit correctament!", Toast.LENGTH_SHORT).show();
                        //I obre el menú principal
                        intent = new Intent(getApplicationContext(), DietKeeper.class);
                        onSaveInstanceState(bundle);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        onStop();
                    } else {
                        if (btLogin.isPressed())
                            Toast.makeText(getApplicationContext(), "Benvingut, " + etUsuari.getText().toString() + "!", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(getApplicationContext(), "L'usuari ja existeix!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "No s'ha pogut connectar a la base de dades...", Toast.LENGTH_LONG).show();
            }
        });
        return loginResponseCall;
    }

    public void onSaveInstanceState(Bundle extras) {
        super.onSaveInstanceState(extras);
        extras.putString("usuari", etUsuari.getText().toString());
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.finish();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
