package cat.ieseugeni;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Activitat que crida a la API de la càmera, pàgina oficial de la API de Android.
 */
public class ActivityCamara extends AppCompatActivity {

    static final int REQUEST_TAKE_PHOTO = 1;

    String rutaActualFoto;
    File photoFile;
    Uri photoURI;
    String usuari;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Primer, que faci la foto i la desi.
        dispatchTakePictureIntent();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //Si la activitat no és nul·la...
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Crea el fitxer, i executa la creació de bytes d'informació referents a la foto.
            photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getApplicationContext(), "Error al crear la fotografia...", Toast.LENGTH_SHORT).show();
            }
            // Si s'ha creat la foto satisfactòriament: Comença una nova activitat.
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getApplicationContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //Se li passa el codi de petició, i la URI obtinguda al mètode getUriForFile...
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            //S'afegeix la foto a la galeria (no funciona encara...)
            afegirFotoGaleria();
            //S'estableix la activitat nova... I s'obté l'usuari, i s'envia tot a ActivityDades.
            data = new Intent(ActivityCamara.this, ActivityDades.class);
            bundle = getIntent().getBundleExtra("bundle2");
            bundle.getString("user");
            data.putExtra("bundle3",bundle);
            startActivity(data);
            onStop();
        }
    }

    //Crea el fitxer a /data/Android/cat.ieseugeni/foto. Amb el format de data de sota.
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Guarda el fitxer de foto amb dades interpretables.
        rutaActualFoto = image.getAbsolutePath();
        return image;
    }

    public void onRestoreInstanceState(Bundle extras) {
        super.onRestoreInstanceState(extras);
        usuari = extras.getString("usuari");
    }

    public void onSaveInstanceState(Bundle extras) {
        super.onSaveInstanceState(extras);
        extras.putString("user", extras.getString("usuari"));
    }

    //Hauria d'afegir la foto a la galeria...
    private void afegirFotoGaleria() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(rutaActualFoto);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}
