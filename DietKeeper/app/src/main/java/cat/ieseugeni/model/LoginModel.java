package cat.ieseugeni.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginModel {

    @SerializedName("id_usuari")
    @Expose
    private Integer id_usuari;
    @SerializedName("usuari")
    @Expose
    private String usuari;
    @SerializedName("pass")
    @Expose
    private String pass;

    public LoginModel() {
    }

    public LoginModel(Integer id, String usuari, String pass) {
        this.id_usuari = id;
        this.usuari = usuari;
        this.pass = pass;
    }

    public Integer getId() {
        return id_usuari;
    }

    public void setId(Integer id) {
        this.id_usuari = id;
    }

    public String getUsuari() {
         return usuari;
    }

    public void setUsuari(String usuari) {
        this.usuari = usuari;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Override
    public String toString() {
        return "LoginModel{" +
                "usuari='" + usuari + '\'' +
                '}';
    }
}
