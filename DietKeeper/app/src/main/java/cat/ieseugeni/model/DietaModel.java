package cat.ieseugeni.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Date;
import java.sql.Time;

public class DietaModel {

    @SerializedName("id_dieta")
    @Expose
    private Integer id_dieta;
    @SerializedName("tipus")
    @Expose
    private String tipus;
    @SerializedName("preu")
    @Expose
    private Float preu;
    @SerializedName("fecha")
    @Expose
    private Date fecha;
    @SerializedName("hora")
    @Expose
    private Time hora;
    @SerializedName("usuari_login")
    @Expose
    private String usuari_login;

    public DietaModel() {

    }

    public DietaModel(Integer id_dieta, String tipus, Float preu, Date fecha, Time hora, String userlogin) {
        this.id_dieta = id_dieta;
        this.tipus = tipus;
        this.preu = preu;
        this.fecha = fecha;
        this.hora = hora;
        this.usuari_login = userlogin;
    }

    public Integer getId_dieta() {
        return id_dieta;
    }

    public void setId_dieta(Integer id_dieta) {
        this.id_dieta = id_dieta;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public Float getPreu() {
        return preu;
    }

    public void setPreu(Float preu) {
        this.preu = preu;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public String getUsuari_login() {
        return usuari_login;
    }

    public void setUsuari_login(String userlogin) {
        this.usuari_login = userlogin;
    }

    @Override
    public String toString() {
        return "DietaModel{" +
                "tipus='" + tipus + '\'' +
                ", preu=" + preu +
                ", fecha=" + fecha +
                ", hora=" + hora +
                ", usuari_login='" + usuari_login + '\'' +
                '}';
    }
}