package cat.ieseugeni.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TipusDietaModel {

    @SerializedName("id_tipus_dieta")
    @Expose
    private Integer id_tipus_dieta;
    @SerializedName("tipus_dieta")
    @Expose
    private String tipus_dieta;

    public TipusDietaModel() {

    }

    public TipusDietaModel(Integer id_tipus_dieta, String tipusdieta) {
        this.id_tipus_dieta = id_tipus_dieta;
        this.tipus_dieta = tipusdieta;
    }

    public Integer getId_tipus_dieta() {
        return id_tipus_dieta;
    }

    public void setId_tipus_dieta(Integer id_tipus_dieta) {
        this.id_tipus_dieta = id_tipus_dieta;
    }

    public String getTipusdieta() {
        return tipus_dieta;
    }

    public void setTipusdieta(String tipusdieta) {
        this.tipus_dieta = tipusdieta;
    }

    @Override
    public String toString() {
        return "TipusDietaModel{" +
                "tipusdieta='" + tipus_dieta + '\'' +
                '}';
    }
}
