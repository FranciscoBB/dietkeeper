package cat.ieseugeni.utils;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Classe per encriptar Strings, o contrasenyes introduïdes.
 * @author fbb
 */
public class CryptoUtils {

    /**
     * Encripta/Genera un hash
     * @param s Text a encriptar
     * @return Text encriptat
     */
    public static String encripta(String s) {
        //En Android, es necessita afegir al davant "new String(Hex.encodeHex(" per a que funcioni.
        return new String(Hex.encodeHex(DigestUtils.sha256(s)));
    }

    /**
     * Comprova si la contrasenya introduïda és la mateixa que el servidor
     * @param bbdd /Contrasenya BBDD
     * @param encriptat /Contrasenya introduïda
     * @return /Contrasenya coincideix
     */
    public static boolean testPassword (String bbdd, String encriptat) {
        return encriptat.equals(bbdd);
    }

    /**
     * Desencripta missatge encriptat mitjançant clau pública
     * @param array /Missatge a desencriptar.
     * @return /Missatge desencriptat
     * @throws InvalidKeyException /Excepció de clau invàlida.
     * @throws NoSuchAlgorithmException /Excepció d'algoritme inexistent.
     * @throws NoSuchPaddingException /Excpeció padding no trobat.
     * @throws IllegalBlockSizeException /Excepció tamany de bloc de la clau.
     * @throws BadPaddingException  /Excpeció padding de la clau.
     */
    public static String desencriptarPublic(byte[] array) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
        SecretKey sKey = null;
        String missatge;
        try {
            byte[] data;
            data = Integer.toString(56).getBytes(StandardCharsets.UTF_8);
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hash = md.digest(data);
            byte[] key = Arrays.copyOf(hash, 128/8);
            sKey = new SecretKeySpec(key, "AES");
        } catch (Exception ex) {
            System.err.println("Error generant la clau:" + ex);
        }

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sKey);
        byte[] decrypted = cipher.doFinal(array);
        return missatge = new String(decrypted);
    }
}
