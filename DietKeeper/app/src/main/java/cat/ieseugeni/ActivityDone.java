package cat.ieseugeni;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

/**
 * Informa a l'usuari que la operació s'ha fet. I ofereix inserir-ne una nova, o sortir.
 */
public class ActivityDone extends AppCompatActivity {

    TextView tvConfirmat;
    Button btMainMenu, btExit;
    Bundle bundlemsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);

        tvConfirmat = findViewById(R.id.tvConfirmat);

        btMainMenu = findViewById(R.id.btMainMenu);
        btExit = findViewById(R.id.btExit);

        bundlemsg = getIntent().getBundleExtra("message");
        tvConfirmat.setText(bundlemsg.getString("success"));

        //Si es vol inserir una nova dieta, s'insereix.
        btMainMenu.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityDone.this,DietKeeper.class);
            startActivity(intent);
            tvConfirmat.setText("");
            onStop();
        });

        btExit.setOnClickListener(v -> {
            onDestroy();
        });
    }

    @Override
    public void onStop(){
        super.onStop();
        this.finish();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        this.finish();
    }
}