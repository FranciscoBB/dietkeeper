package cat.ieseugeni;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

/**
 * Menú principal de la aplicació.
 */
public class DietKeeper extends AppCompatActivity {

    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button camaraButton = findViewById(R.id.camaraButton);
        Button aboutButton = findViewById(R.id.enQuantAButton);

        camaraButton.setOnClickListener(v -> {
            Intent i = new Intent(DietKeeper.this, ActivityCamara.class);
            bundle = getIntent().getBundleExtra("bundle1");
            bundle.getString("user");
            i.putExtra("bundle2",bundle);
            startActivity(i);
            onStop();
        });

        aboutButton.setOnClickListener(v -> {
            Intent i = new Intent(DietKeeper.this, EnQuantA.class);
            startActivity(i);
            onStop();
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.finish();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.finish();
    }
}
