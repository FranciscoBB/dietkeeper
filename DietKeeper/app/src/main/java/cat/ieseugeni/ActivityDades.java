package cat.ieseugeni;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import cat.ieseugeni.api.ApiService;
import cat.ieseugeni.api.NetworkClient;
import cat.ieseugeni.model.DietaModel;
import cat.ieseugeni.model.TipusDietaModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Activitat encarregada d'inserir les dietes dins de la base de dades
 */
public class ActivityDades extends AppCompatActivity {

    private Intent i;
    private static final String TAG = "DIETES";
    private static final String TAG2 = "DIETACOMPLETA";
    EditText casellapreu;
    Spinner spinner;
    Bundle bundle3, bundlemsg;
    private DietaModel dietaModel = new DietaModel();
    private TipusDietaModel tipusDietaModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dades);

        casellapreu = findViewById(R.id.casellaimport);
        Button buttonAcceptar = findViewById(R.id.btOk);
        Button buttonCancelar = findViewById(R.id.btCancelar);
        spinner = findViewById(R.id.spinnertipus);

        obtenirTipusDietes();

        casellapreu.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(4, 2)});

        buttonAcceptar.setOnClickListener(view -> {
            inserirDietes();
        });

        buttonCancelar.setOnClickListener(view -> {
            i = new Intent(getApplicationContext(), ActivityCamara.class);
            startActivity(i);
            onStop();
        });
    }

    /**
     * Crida a la API REST per a poblar el Spinner amb dietes, recorre l'objecte, i extreu la dada del camp.
     * @return Retorna la crida.
     */
    private Call<List<TipusDietaModel>> obtenirTipusDietes() {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        ApiService service = retrofit.create(ApiService.class);
        Call<List<TipusDietaModel>> tipusDietaResponseCall = service.obtenirTipusDietes();
        tipusDietaResponseCall.enqueue(new Callback<List<TipusDietaModel>>() {
            @Override
            public void onResponse(Call<List<TipusDietaModel>> call, Response<List<TipusDietaModel>> response) {

                if (response.isSuccessful()) {

                    List<TipusDietaModel> llistaDietes = response.body();

                    ArrayList<String> dietes = new ArrayList<>();

                    for (int i = 0; i < llistaDietes.size(); i++) {
                        tipusDietaModel = llistaDietes.get(i);
                        dietes.add(tipusDietaModel.getTipusdieta());
                        Log.i(TAG, " Dieta: " + tipusDietaModel.getTipusdieta());
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, dietes);
                    spinner.setAdapter(arrayAdapter);
                    spinner.getOnItemSelectedListener();
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            dietaModel.setTipus(dietes.get(position));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            dietaModel.setTipus(dietes.get(0));
                        }
                    });
                } else {
                    Log.e(TAG, " onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<TipusDietaModel>> call, Throwable t) {
                Log.e(TAG, " onFailure: " + t.getMessage());
            }
        });
        return tipusDietaResponseCall;
    }

    /**
     * Crida a la API REST per a inserir les dietes a partir d'un objecte nou DietaModel.
     * @return Retorna la crida.
     */
    private Call<DietaModel> inserirDietes() {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        ApiService service = retrofit.create(ApiService.class);
        //Data actual del sistema
        long millis = System.currentTimeMillis();

        //Creació de l'objecte DietaModel a partir del constructor buit, se li passen les dades als setters per dins.
        dietaModel.setId_dieta(0);
        dietaModel.setPreu(Float.valueOf(casellapreu.getText().toString()));
        dietaModel.setTipus(dietaModel.getTipus());
        dietaModel.setFecha(new Date(millis));
        dietaModel.setHora(new Time(millis));
        bundle3 = getIntent().getBundleExtra("bundle3");
        //L'usuari s'obté per enviar i rebre el bundle per totes les classes.
        dietaModel.setUsuari_login(bundle3.getString("user"));

        Call<DietaModel> dietaResponseCall = service.inserirDietes(dietaModel);

        dietaResponseCall.enqueue(new Callback<DietaModel>() {
            @Override
            public void onResponse(Call<DietaModel> call, Response<DietaModel> response) {

                if (response.isSuccessful()) {
                    Log.d(TAG2, " Dieta: " + dietaModel.getPreu() + dietaModel.getTipus() + dietaModel.getFecha() + dietaModel.getHora() + dietaModel.getUsuari_login());

                    Toast.makeText(getApplicationContext(), "Dieta inserida correctament!", Toast.LENGTH_SHORT).show();

                    i = new Intent(ActivityDades.this, ActivityDone.class);
                    bundlemsg = new Bundle();
                    bundlemsg.putString("success", getResources().getString(R.string.dietSuccess));
                    i.putExtra("message", bundlemsg);
                    startActivity(i);
                    onStop();
                }
            }

            @Override
            public void onFailure(Call<DietaModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "No s'ha pogut connectar a la base de dades...", Toast.LENGTH_LONG).show();
            }
        });
        return dietaResponseCall;
    }

    public void onRestoreInstanceState(Bundle extras) {
        super.onRestoreInstanceState(extras);
        extras.getString("usuari");
    }

    public void onSaveInstanceState(Bundle extras) {
        super.onSaveInstanceState(extras);
        extras.putString("user", extras.getString("usuari"));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.finish();
    }
}
