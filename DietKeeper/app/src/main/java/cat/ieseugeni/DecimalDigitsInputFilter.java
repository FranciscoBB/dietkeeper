package cat.ieseugeni;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe que estableix el patró del preu, amb dos xifres decimals.
 */
class DecimalDigitsInputFilter implements InputFilter {

    private Pattern pattern;

    public DecimalDigitsInputFilter(int digitsBeforeZero, int digitsAfterZero) {
        pattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero) + "}+((\\.[0-9]{0," + (digitsAfterZero) + "})?)||(\\.)?");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        Matcher matcher = pattern.matcher(dest);
        if(!matcher.matches())
            return "";
        return null;
    }
}
