package cat.ieseugeni.api;

import java.util.List;

import cat.ieseugeni.model.DietaModel;
import cat.ieseugeni.model.LoginModel;
import cat.ieseugeni.model.TipusDietaModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Interfície que conté els mètodes necessaris per a poder interactuar amb la API Rest.
 */
public interface ApiService {

    //Quan es fica {param}, significa que es pot passar un paràmetre com a ruta. Ha de tenir el mateix nom que la propietat del model, i del camp de BBDD.
    @GET("login/{usuari}/{pass}")
    Call<List<LoginModel>> compararLogin(@Path("usuari") String usuari, @Path("pass") String pass);

    //Petició que he fet servir per a poblar el spinner (combobox d'Android).
    @GET("tipusdieta")
    Call<List<TipusDietaModel>> obtenirTipusDietes();

    //En aquest cas, és inserir tot l'objecte JSON en cru de LoginModel.
    @Headers({
            "Content-Type:application/json"
    })
    @POST("login")
    Call<LoginModel> inserirLogin(@Body LoginModel loginModel);

    //El mateix va per a dietes.
    @Headers({
            "Content-Type:application/json"
    })
    @POST("dieta")
    Call<DietaModel> inserirDietes(@Body DietaModel dietaModel);
}
