package cat.ieseugeni.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Patró Singleton que retorna la construcció del objecte Retrofit, per a tractar el fluxe de JSON/objectes.
 */
public class NetworkClient {

    //URL fixa.
    public static final String BASE_URL = "http://mestral.ddns.net:50000/api/";
    public static Retrofit retrofit;

    public static Retrofit getRetrofitClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }
}
