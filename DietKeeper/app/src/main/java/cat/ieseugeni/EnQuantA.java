package cat.ieseugeni;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

/**
 * Informa sobre el creador de la Aplicació.
 */
public class EnQuantA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_en_quant_a);

        findViewById(R.id.tvMissatge1_1);
        findViewById(R.id.tvMissatge1_2);
        findViewById(R.id.tvMissatge2_1);
        findViewById(R.id.tvMissatge2_2);
        findViewById(R.id.tvMissatge3_1);
        findViewById(R.id.tvMissatge3_2);
        findViewById(R.id.tvMissatge4_1);
        findViewById(R.id.ivLogoAbout);
        findViewById(R.id.ivLogoApp);
        Button btAbout = findViewById(R.id.btAbout);

        btAbout.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DietKeeper.class);
            startActivity(intent);
            onStop();
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        this.finish();
    }
}
